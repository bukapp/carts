FROM node:latest

WORKDIR /usr/src/app

COPY . . 

USER root

RUN npm install

RUN npm run build

EXPOSE 3001

ENTRYPOINT [ "npm", "run", "start" ]
